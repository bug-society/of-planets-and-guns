// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Weapons/Guns/OPAG_GunStats.h"
#include "Weapons/Modules/OPAG_ModuleStats.h"
#include "OPAG_UWNewModuleView.generated.h"

enum class EModuleTypes : uint8;
enum class EModuleRarity : uint8;
class UImage;
class UTextBlock;
class UListView;
class UHorizontalBox;
class UVerticalBox;
class UVerticalBoxSlot;
class UTexture2D;

/**
 * 
 */
UCLASS()
class OFPLANETSANDGUNS_API UOPAG_UWNewModuleView : public UUserWidget
{
	GENERATED_BODY()

	UPROPERTY(meta = (BindWidget))
		UTextBlock* ModuleRarityText;
	UPROPERTY(meta = (BindWidget))
		UTextBlock* ModuleTypeText;
	UPROPERTY(meta = (BindWidget))
		UTextBlock* ModuleNameText;
	UPROPERTY(meta = (BindWidget))
		UTextBlock* EquippableText;

	UPROPERTY(meta = (BindWidget))
		UListView* StatsListView;

	UPROPERTY(meta = (BindWidget))
		UVerticalBox* AbilityBox;
	UPROPERTY(meta = (BindWidget))
		UVerticalBox* FatherVerticalBox;
	UPROPERTY(meta = (BindWidget))
		UImage* ThirdBar;

	UPROPERTY(meta = (BindWidget))
		UHorizontalBox* EquippableTypesBox;

	UPROPERTY(meta = (BindWidget))
		UImage* ModuleBackground; 
	UPROPERTY(meta = (BindWidget))
		UImage* ModuleForeground;
	
	UPROPERTY(meta = (BindWidget))
		UImage* ModuleBrandImage;

public:

	//Weapons textures padding
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float WeaponIconsPadding;

	//Weapons textures
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UTexture2D* FullyAutomaticIconTexture;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UTexture2D* Burst3IconTexture;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UTexture2D* ShotgunIconTexture;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UTexture2D* SingleShotIconTexture;

	// Brand textures
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UTexture2D* BlobiusBrandTexture;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UTexture2D* KaponianBrandTexture;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UTexture2D* UronianBrandTexture;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UTexture2D* StandardBrandTexture;

	//UI Colors
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FLinearColor BackgroundBlue;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FLinearColor ForegroundBlue;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FLinearColor BackgroundRed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FLinearColor ForegroundRed;

	//UI Font
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FSlateFontInfo AbilityTextFont;

	//UI Rarity font color
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FSlateColor CommonColor;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FSlateColor UncommonColor;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FSlateColor RareColor;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FSlateColor EpicColor;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FSlateColor LegendaryColor;
	

	UFUNCTION()
	void UpdateModuleValues(const FOPAG_GunStats& CurrentModuleStats, const FOPAG_GunStats& OtherModuleStats,
	                        const FString OtherModuleName, const FString OtherModuleAbilityName, const EModuleRarity OtherModuleRarity, const EModuleTypes OtherModuleType,
	                        const EModuleBrand OtherModuleBrand, const int EquippableTypes, const bool CanBeEquipped);

	void NativeConstruct();

	void AddStatEntry(FString StatName, float OldStat, float NewStat, bool bIsInverted = false);
	void AddStatEntry(FString StatName, int OldStat, int NewStat, bool bIsInverted = false);
};

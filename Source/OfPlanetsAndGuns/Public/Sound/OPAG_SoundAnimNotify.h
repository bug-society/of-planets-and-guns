// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "FMODAnimNotifyPlay.h"
#include "OPAG_SoundAnimNotify.generated.h"

/**
 * 
 */
UCLASS()
class OFPLANETSANDGUNS_API UOPAG_SoundAnimNotify : public UFMODAnimNotifyPlay
{
	GENERATED_BODY()
	
};

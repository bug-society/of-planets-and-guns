#pragma once

#include "CoreMinimal.h"
#include "Characters/Enemies/MeleeEnemy/OPAG_MeleeEnemy.h"
#include "OPAG_DashingEnemy.generated.h"

UCLASS()
class OFPLANETSANDGUNS_API AOPAG_DashingEnemy : public AOPAG_MeleeEnemy
{
	GENERATED_BODY()

	// virtual void BeginPlay() override;
	// virtual void OnAttackBegin(UAnimMontage* Montage) override;
	// virtual void OnAttackEnd(UAnimMontage* Montage, bool bInterrupted) override;
};
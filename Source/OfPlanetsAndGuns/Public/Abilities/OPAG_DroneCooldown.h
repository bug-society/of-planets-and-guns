#pragma once

#include "CoreMinimal.h"
#include "GameplayEffect.h"
#include "OPAG_DroneCooldown.generated.h"

UCLASS()
class OFPLANETSANDGUNS_API UOPAG_DroneCooldown : public UGameplayEffect
{
	GENERATED_BODY()
	
public:
	UOPAG_DroneCooldown();
};

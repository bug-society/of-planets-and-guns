#pragma once

#include "CoreMinimal.h"
#include "GameplayEffect.h"
#include "OPAG_BasicHitDamage.generated.h"

UCLASS()
class OFPLANETSANDGUNS_API UOPAG_BasicHitDamage : public UGameplayEffect
{
	GENERATED_BODY()
	
public:
	UOPAG_BasicHitDamage();
};

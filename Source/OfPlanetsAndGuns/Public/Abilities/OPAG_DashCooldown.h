#pragma once

#include "CoreMinimal.h"
#include "GameplayEffect.h"
#include "OPAG_DashCooldown.generated.h"

UCLASS()
class OFPLANETSANDGUNS_API UOPAG_DashCooldown : public UGameplayEffect
{
	GENERATED_BODY()

public:
	UOPAG_DashCooldown();
};

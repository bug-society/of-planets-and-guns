// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "OPAG_BaseGameMode.generated.h"

/**
 * 
 */
UCLASS()
class OFPLANETSANDGUNS_API AOPAG_BaseGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AOPAG_BaseGameMode();
};
